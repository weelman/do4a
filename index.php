<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="rights" content="LITVINENKO DIGITAL">
    <meta name=«title» content="DO4A.GYM — фитнес-центр в Иркутске.">
    <meta name="description" content="Иркутск, займитесь фитнесом с командой DO4AGYM! Тренажерный зал, групповые программы, тренажеры премиум класса HOIST, линейка спортивного питания и одежды для фитнеса.">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:image" content="/img/oglogo.jpg">
    <meta property="og:url" content="http://do4agym38.ru/">
    <title>DO4A.GYM — фитнес-центр в Иркутске.</title>
    <!--  icon  -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="icon" type="image/png" href="/favicon.ico">
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!--  css   -->
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/burger.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel='stylesheet' id='css-font-awesome-css' href='//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css?ver=4.5.14' type='text/css' media='all' />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmW-57e-FdddBuUb9plS2MonzYbHDdmB8"></script>
    <script>
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 15, // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(52.252885, 104.260758),
            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{ "featureType": "all" , "elementType": "labels.text.fill" , "stylers": [{ "saturation": 36 }, { "color": "#000000" }, { "lightness": 40 }] }, { "featureType": "all" , "elementType": "labels.text.stroke" , "stylers": [{ "visibility": "on" }, { "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "all" , "elementType": "labels.icon" , "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative" , "elementType": "geometry.fill" , "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "administrative" , "elementType": "geometry.stroke" , "stylers": [{ "color": "#000000" }, { "lightness": 17 }, { "weight": 1.2 }] }, { "featureType": "landscape" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "poi" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 21 }] }, { "featureType": "road.highway" , "elementType": "geometry.fill" , "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }, { "featureType": "road.highway" , "elementType": "geometry.stroke" , "stylers": [{ "color": "#000000" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 18 }] }, { "featureType": "road.local" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "transit" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 19 }] }, { "featureType": "water" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }] };
        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');
        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);
        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(52.252885, 104.260758)
            , map: map
            , icon: {
                url: "img/map/map-marker.png"
                , scaledSize: new google.maps.Size(225, 165)
            }
        });
    }
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49117123 = new Ya.Metrika2({
                    id:49117123,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49117123" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>document.documentElement.className = 'js';</script>
</head>
<body class="overfloff">
    <div class="preload">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-12 text-center bonus">
                    <img src="/img/preload.gif" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="bonus_card bc">
        <div class="container">
            <div class="row block_f">
                <div class="col-md-4 col-2 align-self-end">
                    <img class="builder" src="img/card_life/1.jpg" alt="">
                </div>
                <div class="col-md-7 col-9 align-self-center">
                    <p class="first">ну Здравствуй, хлюпик!</p>
<!--                    <p class="second">мы рады твоему визиту</p>-->
                    <p class="third">у меня есть 100 клубных карт
                    <br>на которые я даю 65% скидку.
                    <br>жми кнопку и действуй</p>
                    <button>забрать скидку 65%</button>
                </div>
                <div class="col-1">
                    <img src="img/close-w.png" alt="" class="closer">
                </div>
            </div>
        </div>
    </div>
    <div class="bonus_card__2 bc">
        <div class="container">
            <div class="row block_s">
                <div class="col-3">
                    <img class="builder" src="img/card_life/2.jpg" alt="">
                </div>
                <div class="col-8">
                    <div class="row">
                        <div class="col-12">
                            <p class="first">Остался последний шаг!</p>
                        </div>
                        <div class="col-6">
                            <p class="second">заполни форму ></p>
                        </div>
                        <div class="col-5 martop">
                            <form class="saleform" method="post">
                                <div class="group">
                                    <input type="text" name="name" class="name_hid" placeholder="-" autocomplete="off" required>
                                    <span class="bar"></span>
                                    <label>Имя*</label>
                                </div>
                                <div class="group">
                                    <input type="tel" name="phone" class="input--tel" autocomplete="off" minlength="16" placeholder="-" required>
                                    <span class="bar"></span>
                                    <label>Телефон*</label>
                                </div>
                                <button>ЗАБРАТЬ СКИДКУ</button>
                                <p class="political_par">
                                    <input type="checkbox" id="test1" checked required>
                                    <label for="test1">я даю согласие на обработку <a href="/legal/">персональных данных</a></label>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-1 text-right">
                    <img src="img/close-w.png" alt="" class="closer">
                </div>
            </div>
            <div class="row block_s2">
                <div class="col-11">
                    <p class="first">Остался последний шаг!</p>
                    <p class="second">заполни форму ></p>
                </div>
                <div class="col-1"><img src="img/close-w.png" alt="" class="closer"></div>
                <div class="col-6 align-self-end">
                    <img class="builder w-100" src="img/card_life/mob2.png" alt="">
                </div>
                <div class="col-6">
                    <form class="saleform" method="post">
                        <div class="group">
                            <input type="text" name="name" class="name_hid" placeholder="-" autocomplete="off" required>
                            <span class="bar"></span>
                            <label>Имя*</label>
                        </div>
                        <div class="group">
                            <input type="tel" name="phone" class="input--tel" autocomplete="off" minlength="16" placeholder="-" required>
                            <span class="bar"></span>
                            <label>Телефон*</label>
                        </div>
                        <button>ЗАБРАТЬ СКИДКУ</button>
                        <p class="political_par">
                            <input type="checkbox" id="test7" checked required>
                            <label for="test7">я даю согласие на обработку <a href="/legal/">персональных данных</a></label>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden_info">
        <div class="container vh-100">
            <div class="row vh-100">
                <div class="col-6 align-self-start">
                    <img class="logo" src="img/header/logo.svg" alt="">
                </div>
                <div class="col-6 align-self-start text-right">
                    <div class="closer">
                        <img src="img/close-w.png" alt="" class="close">
                    </div>
                </div>
                <div class="col-12 align-self-start">
                    <p>Одежда для фитнеса и спортивного зала является неотъемлемой частью спортивного процесса. Правильно подобранная одежда для фитнеса и спортивного зала способна не только украсить спортивный выход, но и сделать тренировку максимально удобной, а значит эффективной. О том, как подбирать одежду для фитнеса и спортивного зала, и как ухаживать за одеждой для фитнеса и спортивного зала вы можете узнать у консультантов клуба DO4A GYM.</p>
                </div>
                <div class="col-12 align-self-end images">
                    <div class="row">
                        <div class="col-3"><img class="w-100 hi_1" src="/img/plus/1/1.jpg" alt=""></div>
                        <div class="col-3"><img class="w-100 hi_2" src="/img/plus/1/2.jpg" alt=""></div>
                        <div class="col-3"><img class="w-100 hi_3" src="/img/plus/1/3.jpg" alt=""></div>
                        <div class="col-3"><img class="w-100 hi_4" src="/img/plus/1/4.jpg" alt=""></div>
                    </div>
                </div>
                <div class="col-6 onas_arrow align-self-center">
                    <div class="onas_left" data-where="">
                        <img src="img/block-1/right-arrow.svg" alt="">
                    </div>
                    <div class="onas_right" data-where="">
                        <img src="img/block-1/right-arrow.svg" alt="">
                    </div>
                </div>
                <div class="col-6 align-self-center text-right"><a href="" class="gocard">ПРИОБРЕСТИ КЛУБНУЮ КАРТУ</a></div>
            </div>
        </div>
    </div>
    <div class="hidden_cardbye">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 text-center theform">
                    <div class="closer">
                        <img src="img/close-w.png" alt="" class="close">
                    </div>
                    <h2>заполните форму</h2>
                    <p>мы оперативно с вами<br>свяжемся и заведем<br>вам клубную карту</p>
                    <div class="row justify-content-center">
                        <div class="col-md-4 col-10">
                            <form method="POST">
                                <input type="text" name="cardtype" class="cardtype d-none" value="Не выбрал карту!">
                                <div class="group">
                                    <input type="text" class="name_hid" name="name" autocomplete="off" placeholder="-" required>
                                    <span class="bar"></span>
                                    <label>Имя*</label>
                                </div>
                                <div class="group">
                                    <input type="text" name="surname" placeholder="-" autocomplete="off">
                                    <span class="bar"></span>
                                    <label>Фамилия</label>
                                </div>
                                <div class="group">
                                    <input type="tel" name="phone" class="input--tel" autocomplete="off" placeholder="-" minlength="16" required>
                                    <span class="bar"></span>
                                    <label>Телефон*</label>
                                </div>
                                <div class="group">
                                    <input type="email" placeholder="-" name="email" autocomplete="off">
                                    <span class="bar"></span>
                                    <label>Email</label>
                                </div>
                                <button>ОСТАВИТЬ ЗАЯВКУ</button>
                                <p class="political_par">
                                    <input type="checkbox" id="checkbox2" checked required>
                                    <label for="checkbox2">я даю согласие на обработку <a href="/legal/">персональных данных</a></label>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-center success d-none">
                    <h2>Cпасибо!</h2>
                    <p>Ваша заявка принята.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="gobonus d-none">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-12 text-center bonus">
                    <img src="/img/bonus.gif" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="block-1">
        <header>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-3 col-6">
                        <a href="/"><img src="img/header/logo.svg" class="w-100" alt=""></a>
                    </div>
                    <div class="col-md-9 d-md-block d-none">
                        <div class="row justify-content-between">
                            <div class="col-auto">
                                <a href="#map" class="address link">
                                    <img src="img/header/address_icon.svg" alt="" class="icon">
                                    ЮРИЯ ТЕНА 25
                                </a>
                            </div>
                            <div class="col-auto">
                                <a href="#onas" class="link">О НАС</a>
                            </div>
                            <div class="col-auto">
                                <a href="" class="gocard link">КУПИТЬ КАРТУ</a>
                            </div>
                            <div class="col-auto">
                                <a href="#contact" class="link">КОНТАКТЫ</a>
                            </div>
                            <div class="col-auto">
                                <a href="tel:+73952600200" class="phone"><img src="img/header/phone-receiver.svg" alt="" class="icon">600-200</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 d-md-none d-block">
                        <div class="menu">
                            <span class="bar"></span>
                            <span class="bar"></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="col-12 hidmenu text-right" style="display: none">
            <a href="tel:+73952600200">600-200</a>
            <a href="#map">ЮРИЯ ТЕНА 25</a>
            <a href="" class="gocard">КОНСУЛЬТАЦИЯ</a>
        </div>
        <div class="underheader">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1>Открытие фитнес-центра do4a в Иркутске</h1>
                        <div class="owl-carousel ow-desc">
                            <div>
<!--                            <a href="">-->
                            <img src="img/block-1/1.jpg" alt="">
<!--                            </a>-->
                            <img src="img/block-1/card.png" alt="" class="card" data-type="Первая карта"></div>
                            <div>
<!--                            <a href="">-->
                            <img src="img/block-1/2.jpg" alt="">
<!--                            </a>-->
                            <img src="img/block-1/card.png" alt="" class="card" data-type="Вторая карта"></div>
                            <div>
<!--                            <a href="">-->
                            <img src="img/block-1/3.jpg" alt="">
<!--                            </a>-->
                            <img src="img/block-1/card.png" alt="" class="card" data-type="Третья карта"></div>
                            <div>
<!--                            <a href="">-->
                            <img src="img/block-1/4.jpg" alt="">
<!--                            </a>-->
                            <img src="img/block-1/card.png" alt="" class="card" data-type="Четвертая карта"></div>
                        </div>
                        <div class="owl-carousel ow-mob">
                            <div>
                                <img src="img/mobslide/2.jpg" alt="">
                                <img src="img/block-1/card.png" alt="" class="card" data-type="Вторая карта">
                            </div>
                            <div>
                                <img src="img/mobslide/1.jpg" alt="">
                                <img src="img/block-1/card.png" alt="" class="card" data-type="Первая карта">
                            </div>
                            <div>
                                <img src="img/mobslide/3.jpg" alt="">
                                <img src="img/block-1/card.png" alt="" class="card" data-type="Третья карта">
                            </div>
                            <div>
                                <img src="img/mobslide/4.jpg" alt="">
                                <img src="img/block-1/card.png" alt="" class="card" data-type="Четвертая карта"></div>
                        </div>
                    </div>
                    <div class="col-12 arrowparty">
                        <div class="arrow_left">
                            <img src="img/block-1/right-arrow.svg" alt="">
                        </div>
                        <div class="arrow_right">
                            <img src="img/block-1/right-arrow.svg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-2" id="onas">
        <div class="container">
            <div class="row justify-content-center grid grid--effect-vega">
                <div class="col-12">
                    <h2><mobbr>волшебных</mobbr> пилюль не бывает</h2>
                    <p>do4agym — это место победителя</p>
                </div>
                <div class="col-md-4 col-12">
                    <div class="titlsgo" data-tilt data-count="1">
                        <img src="img/block-2/1.jpg" alt="Image"/>
                        <span>общая Площадь 1200 к.м</span>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="titlsgo" data-tilt data-count="2">
                        <img src="img/block-2/2.jpg" alt="Image"/>
                        <span>Оборудование премиум класса</span>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="titlsgo" data-tilt data-count="3">
                        <img src="img/block-2/3.jpg" alt="Image"/>
                        <span>Персониф. гантельный ряд</span>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="titlsgo" data-tilt data-count="4">
                        <img src="img/block-2/4.jpg" alt="Image"/>
                        <span>большая кардио зона</span>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="titlsgo" data-tilt data-count="5">
                        <img src="img/block-2/5.jpg" alt="Image"/>
                        <span>Боксерский ринг прямо в зале</span>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="titlsgo" data-tilt data-count="6">
                        <img src="img/block-2/6.jpg" alt="Image"/>
                        <span>Массажный кабинет и солярий</span>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="titlsgo" data-tilt data-count="7">
                        <img src="img/block-2/7.jpg" alt="Image"/>
                        <span>линейка спортивного питания</span>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="titlsgo" data-tilt data-count="8">
                        <img src="img/block-2/8.jpg" alt="Image"/>
                        <span>линейка одежды для фитнеса</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <h2>узнайте стоимость
                    <br>клубной карты
                    <br>прямо сейчас</h2>
                </div>
                <div class="col-md-4 col-10">
                    <form class="form_b3" method="POST">
                       <input type="text" name="cardtype" class="cardtype d-none" value="Узнать стоимость карты">
                        <div class="group">
                            <input type="text" class="name_hid" name="name" autocomplete="off" placeholder="-" required>
                            <span class="bar"></span>
                            <label>Имя*</label>
                        </div>
                        <div class="group">
                            <input type="text" name="surname" autocomplete="off" placeholder="-">
                            <span class="bar"></span>
                            <label>Фамилия</label>
                        </div>
                        <div class="group">
                            <input type="tel" name="phone" class="input--tel" placeholder="-" minlength="16" autocomplete="off" required>
                            <span class="bar"></span>
                            <label>Телефон*</label>
                        </div>
                        <div class="group">
                            <input type="email" name="email" autocomplete="off" placeholder="-">
                            <span class="bar"></span>
                            <label>Email</label>
                        </div>
                        <button>ОСТАВИТЬ ЗАЯВКУ</button>
                        <p class="political_par">
                            <input type="checkbox" id="checkbox3" checked required>
                            <label for="checkbox3">я даю согласие на обработку <a href="/legal/">персональных данных</a></label>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="block-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">Что ты получаешь?</h2>
                </div>
            </div>
        </div>
        <div class="bg1">
            <div class="container h-100">
                <div class="row align-items-center h-100">
                    <div class="col-12 d-md-none d-block">
                        <img src="img/block-4/mob1.jpg" alt="" class="w-100">
                    </div>
                    <div class="col-md-7 offset-md-5 col-12 text-right">
                        <h3>поддержку сильных наставников</h3>
                        <p>В зале DO4AGYM ты находишься среди опытных спортсменов,<br>которые уже достигли определенного результата и готовы помочь<br>тебе справиться с трудностями и ответить на все вопросы. В таком<br>обществе ты гораздо быстрее начнешь свое спортивное развитие.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg2">
            <div class="container h-100">
                <div class="row align-items-center h-100">
                    <div class="col-12 d-md-none d-block">
                        <img src="img/block-4/mob2.jpg" alt="" class="w-100">
                    </div>
                    <div class="col-md-7 col-12">
                        <h3>второй дом</h3>
                        <p>Да - да, ты не ослышался! Тебя здесь любят и ждут,</p><p>поэтому считай это своим вторым домом.</p><p>Чувствуй себя как дома, ты здесь не в гостях, это твой зал.</p>
                        <p class="margintop">Здесь тебя накормят, ты отлично потренируешься с друзьями, выйдешь на новые результаты и станешь красивее, счастливее и сильнее.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-12">
                    <h3 class="margh3">Ты становишься полноценным членом большой семьи do4agym</h3>
                </div>
                <div class="col-md-10 col-12">
                    <h2 class="text-center">в честь нашего знакомства,<br>я дарю тебе программу тренировок</h2>
                </div>
                <div class="col-md-8 col-12">
                    <div class="row align-items-center sn">
                        <div class="col-md-6 col-12">
                            <div class="sportnutrition">
                                <img src="img/block-5/1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-9 offset-3 offset-md-0">
                            <div>
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" data-img="1.jpg" checked>
                                <label for="radio-1" class="radio-custom-label">ПОДСУШИТЬСЯ</label>
                            </div>
                            <div>
                                <input id="radio-2" class="radio-custom" name="radio-group" type="radio" data-img="2.jpg">
                                <label for="radio-2" class="radio-custom-label">НАБРАТЬ МАССУ</label>
                            </div>
                            <div>
                                <input id="radio-3" class="radio-custom" name="radio-group" type="radio" data-img="3.jpg">
                                <label for="radio-3" class="radio-custom-label">УВЕЛИЧИТЬ СИЛУШКУ</label>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button class="getprogramm">получить программу</button>
                        </div>
                    </div>
                    <form action="" method="POST" class="w-100 sninput d-none">
                        <div class="row align-items-center justify-content-center">
                           <div class="d-md-none col-12">
                                <div class="sportnutrition">
                                    <img src="img/block-5/1.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 col-10 text-center">
                                <div class="group">
                                    <input type="text" name="name" class="name_hid" autocomplete="off" placeholder="-" required>
                                    <span class="bar"></span>
                                    <label>Имя*</label>
                                </div>
                                <div class="group">
                                    <input type="tel" name="phone" class="input--tel" minlength="16" autocomplete="off" placeholder="-">
                                    <span class="bar"></span>
                                    <label>Телефон</label>
                                </div>
                                <div class="group">
                                    <input type="email" name="email" autocomplete="off" placeholder="-" required>
                                    <span class="bar"></span>
                                    <label>Email*</label>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button>ОСТАВИТЬ ЗАЯВКУ</button>
                            </div>
                            <p class="political_par">
                                <input type="checkbox" id="checkbox4" checked required>
                                <label for="checkbox4">я даю согласие на обработку <a href="/legal/">персональных данных</a></label>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="block-6"></div>
    <div class="block-7" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">забронируй клубную карту</h2>
                    <button class="gocard">прямо сейчас</button>
                </div>
            </div>
        </div>
    </div>
    <div class="socnetwork">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h2>подписывайся</h2>
                    <span>#do4agym38 #do4ateam
                    <br>#КАЧАЙИРКУТСК</span>
                    <div class="row">
                        <div class="col-md-1 offset-md-4 col-2 offset-2">
                            <div class="nolag">
                                <div class="round_network"><i class="icon-vk"></i></div>
                                <a href="https://vk.com/do4agym38" rel="nofollow" target="_blank">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1 col-2">
                            <div class="nolag">
                                <div class="round_network"><i class="icon-instagram"></i></div>
                                <a href="https://www.instagram.com/do4agym38/" rel="nofollow" target="_blank">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1 col-2">
                            <div class="nolag">
                                <div class="round_network"><i class="icon-youtube"></i></div>
                                <a href="https://www.youtube.com/channel/UCRbndTIEXJTpISxnIyBfqDQ?view_as=subscriber" rel="nofollow" target="_blank">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1 col-2">
                            <div class="nolag">
                                <div class="round_network"><i class="icon-facebook"></i></div>
                                <a href="https://www.facebook.com/DO4A-GYM-1642277049161509/" rel="nofollow" target="_blank">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-map">
        <h2 class="text-center"><mobbr>заезжай к нам</mobbr> в фитнес-центр</h2>
        <div id="map"></div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-4 developers">
                    <a href="http://litvinenko.digital" target="_blank"><img src="/img/logo_litvinenko.svg" alt=""></a><br>
                </div>
                <div class="col-4 text-center">
                    <a href="/legal/" class="link">Политика конфиденциальности</a>
                </div>
                <div class="col-4 text-right">
                    <p>©<a href="http://do4agym38.ru">DO4A.GYM38</a> 2018</p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.2.1/tilt.jquery.min.js"></script>
    <script src="js/index.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function(){$('.preload').addClass('whitepower'); $('.whitepower').addClass('gogobonuspower');
            $('body').removeClass('overfloff');}, 1800);
            setTimeout(function(){$('.preload').hide();}, 3300);
            $('.ow-desc').owlCarousel({
                loop:true,
                autoplay:true,
                autoplayTimeout:4000,
                autoplayHoverPause:true,
                margin:10,
                smartSpeed: 1000,
                dots: false,
                items:1
            });
            var owl = $('.ow-desc');
            if ($(window).width()<480) {
                $('.ow-mob').owlCarousel({
                    loop:true,
                    autoplay:true,
                    autoplayTimeout:4000,
                    autoplayHoverPause:true,
                    margin:10,
                    smartSpeed: 1000,
                    dots: false,
                    items:1
                });
                owl = $('.ow-mob');
            }
            owl.owlCarousel();
            $(document).on("click", ".arrow_left", function(){
                owl.trigger('prev.owl.carousel');
            });
            $(document).on("click", ".arrow_right" ,function(){
                owl.trigger('next.owl.carousel');
            });
            if ($(window).width()<900) {
                const tilt = $('.titlsgo').tilt();
                tilt.tilt.destroy.call(tilt);
            }
        });
    </script>
</body>
</html>
