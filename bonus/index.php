<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="rights" content="">
    <meta name=«title» content="Бонус">
    <meta name="description" content="">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:url" content="">
    <title></title>
    <!--  icon  -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="icon" type="image/png" href="/favicon.ico">
    <link rel="apple-touch-icon" href="/favicon.ico">
    <!--  css   -->
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="../css/burger.css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <link rel='stylesheet' id='css-font-awesome-css' href='//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css?ver=4.5.14' type='text/css' media='all' />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmW-57e-FdddBuUb9plS2MonzYbHDdmB8"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49117123 = new Ya.Metrika2({
                    id:49117123,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
    <script>
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 15, // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(52.252885, 104.260758),
                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{ "featureType": "all" , "elementType": "labels.text.fill" , "stylers": [{ "saturation": 36 }, { "color": "#000000" }, { "lightness": 40 }] }, { "featureType": "all" , "elementType": "labels.text.stroke" , "stylers": [{ "visibility": "on" }, { "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "all" , "elementType": "labels.icon" , "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative" , "elementType": "geometry.fill" , "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "administrative" , "elementType": "geometry.stroke" , "stylers": [{ "color": "#000000" }, { "lightness": 17 }, { "weight": 1.2 }] }, { "featureType": "landscape" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "poi" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 21 }] }, { "featureType": "road.highway" , "elementType": "geometry.fill" , "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }, { "featureType": "road.highway" , "elementType": "geometry.stroke" , "stylers": [{ "color": "#000000" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 18 }] }, { "featureType": "road.local" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "transit" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 19 }] }, { "featureType": "water" , "elementType": "geometry" , "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }] };
            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');
            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(52.252885, 104.260758)
                , map: map
                , icon: {
                    url: "../img/map/map-marker.png"
                    , scaledSize: new google.maps.Size(225, 165)
                }
            });
        }
    </script>
</head>
<body>
   <div class="gobonus whitepower"></div>
    <div class="block-1">
        <header>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-3 col-6">
                        <a href="/"><img src="../img/header/logo.png" class="w-100" alt=""></a>
                    </div>
                    <div class="col-md-9 d-md-block d-none">
                        <div class="row justify-content-center">
                            <div class="col-2">
                            </div>
                            <div class="col-auto">
                                <a href="/" class="link">&#60; НА ГЛАВНУЮ</a>
                            </div>
                            <div class="col-auto">
                                <a href="tel:+73952600200" class="\"><img src="../img/header/phone-receiver.svg" alt="" class="icon">600-200</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 d-md-none d-block">
                        <div class="menu">
                            <span class="bar"></span>
                            <span class="bar"></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="col-12 hidmenu text-right" style="display: none">
            <a href="tel:+73952600200">600-200</a>
            <a href="#map">ЮРИЯ ТЕНА 25</a>
            <a href="" class="gocard">НА ГЛАВНУЮ</a>
        </div>
    </div>
    <div class="bonus">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>Поздравляем тебя<?php if(isset($_GET['name'])) echo ', '.($_GET['name']); ?>!
                    <br>Мы рады, что ты теперь с нами.</h2>
                    <p>А Пока наши менеджеры обрабатывают твою заявку, предлагаем
                    <br>посмотреть мультик про героя d04a — виталия пропианата</p>
                </div>
                <div class="col-md-1">

                </div>
                <div class="col-md-10 col-12">
                    <div class="owl-carousel">
                        <div>
                            <div class="row">
                                <div class="col-1">
                                    <img src="../img/bonus/1.jpg" alt="">
                                </div>
                                <div class="col-10">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/vGiVndadvNc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-1">
                                    <img src="../img/bonus/2.jpg" alt="">
                                </div>
                                <div class="col-10">
                                    <iframe width="100%" src="https://www.youtube.com/embed/hDOFjH2hIT4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-1">
                                    <img src="../img/bonus/3.jpg" alt="">
                                </div>
                                <div class="col-10">
                                    <iframe width="100%" src="https://www.youtube.com/embed/qRTqbP9NG0w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-1">
                                    <img src="../img/bonus/4.jpg" alt="">
                                </div>
                                <div class="col-10">
                                    <iframe width="100%" src="https://www.youtube.com/embed/780TQxDYxFo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-1">
                                    <img src="../img/bonus/5.jpg" alt="">
                                </div>
                                <div class="col-10">
                                    <iframe width="100%" src="https://www.youtube.com/embed/2VYQUifQmJ4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-1">
                                    <img src="../img/bonus/6.jpg" alt="">
                                </div>
                                <div class="col-10">
                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/nEOuDjfE4-Y" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 offset-md-2 offset-1 arrowparty">
                    <div class="arrow_left">
                        <img src="../img/block-1/right-arrow.svg" alt="">
                    </div>
                    <div class="arrow_right">
                        <img src="../img/block-1/right-arrow.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-6"></div>
    <div class="socnetwork">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h2>подписывайся</h2>
                    <span>#do4agym38 #do4ateam
                    <br>#КАЧАЙИРКУТСК</span>
                    <div class="row">
                        <div class="col-md-1 offset-md-4 col-2 offset-2">
                            <div class="nolag">
                                <div class="round_network"><i class="icon-vk"></i></div>
                                <a href="https://vk.com/do4agym38" rel="nofollow" target="_blank">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1 col-2">
                            <div class="nolag">
                                <div class="round_network"><i class="icon-instagram"></i></div>
                                <a href="https://www.instagram.com/do4agym38/" rel="nofollow" target="_blank">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1 col-2">
                            <div class="nolag">
                                <div class="round_network"><i class="icon-youtube"></i></div>
                                <a href="https://www.youtube.com/channel/UCRbndTIEXJTpISxnIyBfqDQ?view_as=subscriber" rel="nofollow" target="_blank">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1 col-2">
                            <div class="nolag">
                                <div class="round_network"><i class="icon-facebook"></i></div>
                                <a href="https://www.facebook.com/DO4A-GYM-1642277049161509/" rel="nofollow" target="_blank">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-map">
        <h2 class="text-center"><mobbr>заезжай к нам</mobbr> в фитнес-клуб</h2>
        <div id="map"></div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-4 developers">
                    <a href="http://litvinenko.digital" target="_blank"><img src="/img/logo_litvinenko.svg" alt=""></a><br>
                </div>
                <div class="col-4 text-center">
                    <a href="/legal/" class="link">Политика конфеденциальности</a>
                </div>
                <div class="col-4 text-right">
                    <p>©<a href="http://do4agym38.ru">DO4A.GYM38</a> 2018</p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script>
        $(document).scroll(function(){
            var blockp = $(".block-6").offset().top;
            var wstart = $(window).scrollTop() + $(window).height();
            var widthwin = $(window).width()/6.85;
            var wend = $(window).scrollTop();
            if (blockp < wstart && blockp > wend) {
                var backpos = wstart/(blockp/widthwin)-widthwin;
                $('.block-6').attr('style', 'background-position: center -' + backpos + 'px');
            }
        });
    </script>
    <script src="../js/owl.carousel.min.js"></script>
    <script type="text/javascript">
        //mobile burger

        $('.menu').click(function() {
            if($(this).is('.active:not(.back)')) {
                 $(this).addClass('back');
                $('.hidmenu').slideUp('fast');
            } else if ($(this).is('.back')) {
                $(this).removeClass('back');
                $('.hidmenu').slideDown('fast');
            } else {
                $(this).addClass('active');
                $('.hidmenu').slideDown('fast');
            }
        });

        $(document).ready(function () {
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                dots: false,
                items:1
            });
            var owl = $('.owl-carousel');
            owl.owlCarousel();
            $(document).on("click", ".arrow_left", function(){
                owl.trigger('prev.owl.carousel');
            });
            $(document).on("click", ".arrow_right" ,function(){
                owl.trigger('next.owl.carousel');
            });
            $('.gobonus').addClass('gogobonuspower');
            setTimeout(function(){$('.gobonus').hide()}, 2500);
        });
    </script>
</body>
</html>
