
//mobile burger

$('.menu').click(function() {
	if($(this).is('.active:not(.back)')) {
		 $(this).addClass('back');
        $('.hidmenu').slideUp('fast');
	} else if ($(this).is('.back')) {
		$(this).removeClass('back');
        $('.hidmenu').slideDown('fast');
	} else {
		$(this).addClass('active');
        $('.hidmenu').slideDown('fast');
	}
});


//Scrollweedeveryday

$('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1200, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
    }
  });


//phone mask
$(function () {
  $('.input--tel').mask('+7(999)999-99-99');

  $('.input--tel').on('focus', function () {
     if ($(this).val().length === 0) {
       $(this).val('+7(');
     }
  });
  $('.input--tel').on('focusout', function () {
      if ($(this).val().length <= 4) {
       $(this).val('');
     }
  });

  $('.input--tel').keydown(function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			 (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			 (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			 (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			 (e.keyCode >= 35 && e.keyCode <= 39)) {
				return;
		}

		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
});

$(document).scroll(function(){
    var blockp = $(".block-6").offset().top;
    var wstart = $(window).scrollTop() + $(window).height();
    var widthwin = $(window).width()/1.35;
    var wend = $(window).scrollTop();
    if (blockp < wstart && blockp > wend) {
        var backpos = wstart/(blockp/widthwin)-widthwin;
        $('.block-6').attr('style', 'background-position: center -' + backpos + 'px');
    }
});


//form 1 hidden
function form_animate(who, deal) {
    $(who).animate(
        {
            opacity: deal,
            height: deal
        },
    300);
}
$('.card').mouseup(function(){
    form_animate('.hidden_cardbye', 'show');
    yaCounter49117123.reachGoal('trainer_clock');
    $('html').addClass('overfloff');
    $('.hidden_cardbye .cardtype').val($(this).data('type'));
});
$('.gocard').click(function(){
    form_animate('.hidden_cardbye', 'show');
    return false;
});
$('.hidden_cardbye .closer').click(function(){
    form_animate('.hidden_cardbye', 'hide');
    $('html').removeClass('overfloff');
});
function success_form(name){
    $('.hidden_cardbye .theform').addClass('d-none');
    $('.hidden_cardbye .success').removeClass('d-none');
    $('.hidden_cardbye form').trigger('reset');
    setTimeout(function(){
        $('.hidden_cardbye').addClass('d-none');
        $('.gobonus').removeClass('d-none');
        setTimeout(function(){$('.gobonus').addClass('whitepower')}, 2700);
        setTimeout(function(){location='/bonus?name=' + name;}, 2900);
    }, 1500);
}

//75%
$('.saleform').submit(function(){
    var data_form = $(this).serialize();
    var phone = $(this).find('.input--tel');
    form_animate('.hidden_cardbye', 'show');
    if(phone.val().length!=16){
        alert('Введите номер телефона полностью');
    } else {
        var needname = $('.saleform').find('.name_hid').val();
        yaCounter49117123.reachGoal('send_sale');
        $.ajax({
            type: "POST", //Метод отправки
            url: "/sale_send.php", //путь до php фаила отправителя
            data: data_form,
            success: function() {
                success_form(needname);
            },
            error: function() {
                success_form(needname);
            }
        });
    }
    return false;
});

//узнать стоимость карты
$('.form_b3').submit(function(){
    var data_form = $(this).serialize();
    var phone = $(this).find('.input--tel');
    $('.hidden_cardbye .theform').addClass('d-none');
    $('.hidden_cardbye .success').removeClass('d-none');
    form_animate('.hidden_cardbye', 'show');
    if(phone.val().length!=16){
        alert('Введите номер телефона полностью');
    } else {
        var needname = $('.form_b3').find('.name_hid').val();
        yaCounter49117123.reachGoal('titlsgo');
        $.ajax({
            type: "POST", //Метод отправки
            url: "/first_send.php", //путь до php фаила отправителя
            data: data_form,
            success: function() {
                success_form(needname);
            },
            error: function() {
                success_form(needname);
            }
        });
    }
    return false;
});

//покупка карты
$('.hidden_cardbye form').submit(function(){
    var data_form = $(this).serialize();
    var phone = $(this).find('.input--tel');
    if(phone.val().length!=16){
        alert('Введите номер телефона полностью');
    } else {
        var needname = $('.hidden_cardbye form').find('.name_hid').val();
        $.ajax({
            type: "POST", //Метод отправки
            url: "/first_send.php", //путь до php фаила отправителя
            data: data_form,
            success: function() {
                success_form(needname);
                return false;
            },
            error: function() {
                success_form(needname);
                return false;
            }
        });
    }
    return false;
});

//checked
$('.radio-custom').change(function(){
    var url = $('.radio-custom:checked').data('img');
    $('.sportnutrition img').attr('src', 'img/block-5/' + url);
});
$('.getprogramm').click(function(){
    yaCounter49117123.reachGoal('getprogram');
    $('.sn').addClass('d-none');
    $('.sninput').removeClass('d-none');
});
$('.sninput').submit(function(){
    var data_form = $(this).serialize();
    $('.hidden_cardbye .theform').addClass('d-none');
    $('.hidden_cardbye .success').removeClass('d-none');
    form_animate('.hidden_cardbye', 'show');
    var needname = $(this).find('.name_hid').val();
    yaCounter49117123.reachGoal('goprogram');
    $.ajax({
        type: "POST", //Метод отправки
        url: "/first_send.php", //путь до php фаила отправителя
        data: data_form,
        success: function() {
            success_form(needname);
            return false;
        },
        error: function() {
            success_form(needname);
            return false;
        }
    });
    return false;
});

//about
function titlsgoinfo(num) {
    switch(num) {
        case 1:
            var para = "DO4AGYM — это новый фитнес-центр в Иркутске.<br>1200 квадратных метров: просторный тренажерный зал 700 кв.м. с оборудованием премиум <br>класса фирмы HOIST, с персонифицированным  гантельным рядом, зал груповых занятий,<br>сайкл студия, большая кардио зона, опытные тренера, боксерский ринг, массажный кабинет,<br>фитнес бар, магазин спортивного питания и спортивной одежды.<br>Основные принципы работы центра — это профессионализм, исключительный сервис<br>и уникальные услуги. В DO4A.GYM есть все для того, чтобы мечты стали реальностью.<br>Мы формируем желаение изменить себя и изменить мир к лучшему!<br>Девиз DO4A.GYM: «Волшебных пилюль не бывает!»";
            break;
        case 2:
            var para = "Оборудование и тренажеры премиум класса — HOIST.<br>В DO4A.GYM представлено оборудование американской компании HOIST известной во всем мире<br>как производитель высококачественного оборудования для силовых тренировок. Инновационная<br>система в основе которой лежат естественные движения человека, дает ошеломляющий результат<br>в кратчайшие сроки.<br> Благодаря сбалансированной биомеханике, естественной траектории движений к равномерному <br>сопротивлению, они способны подстраиваться под индивидуальные параметры пользователя.<br>Обеспечивают опору для головы и спины что позволяет разгрузить позвоночник и получить<br>максимальную пользу от тренировок. Технология Silent Steel делает работу тренажеров бесшумной.";
            break;
        case 3:
            var para = "Персонифицированный гантельный ряд.<br>Полный комплект качественных гантелей от мала до велика позволит равномерно увеличивать<br>рабочие веса и полноценно, комфортно заниматься с железом увеличивая свою силушку.<br>Мы позаботились о том, чтобы гантелей хватило на всех! И на тех хлюпиков что халтурят и <br>на тру билдеров, которые пришли к нам побеждать.";
            break;
        case 4:
            var para = "Большая кардио зона.<br>Чтобы добиться результатов в формировании своего красивого тела, необходимо заниматься<br>кардио. Плавно увеличивать выносливость и тренировать свое сердечко. За правильные <br>тренировки ваш организм скажет вам спасибо,  Мы позаботились о наличии широкого выбора: от беговых дорожек до сайкл и эллипсоидов.";
            break;
        case 5:
            var para = "Боксерский ринг в фитнес-центре.<br>В центре нашего зала распололжен фирменный боксерский ринг, на который может взойти<br>любой поcетитесь нашего фитнес-центра DO4A.GYM. Натренировать ударную технику<br>и поспаринговаться. Профессиональная экипировка и опытные тренеры.";
            break;
        case 6:
            var para = "Массажный кабинет и солярий.<br>Массаж является неотъемлемой частью, как тренировочного процесса, так и процессов, связанных<br>с лечением избыточного веса. Прежде всего, массаж - это увеличение крово- и лимфообращения,<br>что в свою очередь, способствует более быстрому восстановлению тканей организма после тренировок<br>и травм.<br>Солярий удачно дополняет занятия на тренажерах, с гантелями, гирями, бег, езду на велосипеде,<br>упражнения йоги и бокса Это возможность всегда быть загорелым, будто бы вы только что<br>вернулись с теплых курортов в Сочи, на Мальте, Кипре на Гавайях или родном Байкале ;)<br>Мускулистое загорелое тело — разве может быть что-то привлекательнее?";
            break;
        case 7:
            var para = "Спортивное питание.<br>Создать гармоничное тело и нарастить эффективную мышечную массу помогут<br>не только интенсивные тренировки, но и качественное спортивное питание.<br>Спортивное питание – это пищевые продукты, направленные на повышение <br>силы и выносливости, укрепления мышц в частности и здоровья в целом. <br>Различают множество видов такой продукции: протеины, гейнеры, аминокислоты <br>и так далее – все они способствуют повышению результатов на тренировках.<br>У нас вы найдете все необходимые добавки, чтобы поддерживать и совершенствовать<br>свою физическую форму.";
            break;
        case 8:
            var para = "Одежда для фитнеса и спортивного зала является неотъемлемой частью спортивного<br>процесса. Правильно подобранная одежда для фитнеса и спортивного зала способна<br>не только украсить спортивный выход, но и сделать тренировку максимально удобной,<br>а значит эффективной. О том, как подбирать одежду для фитнеса и спортивного зала,<br>и как ухаживать за одеждой для фитнеса и спортивного зала вы можете узнать у <br>консультантов клуба DO4A GYM.";
            break;
    }
    $('.hidden_info p').html(para);
    if(num==1){
        $('.onas_left').data("where", 8);
    } else {
        $('.onas_left').data("where", num-1);
    }
    if(num==8){
        $('.onas_right').data("where", 1);
    } else {
        $('.onas_right').data("where", num+1);
    }
    var par = 0;
    for(var i=0; i<4; i++) {
        var par = par+1;
        var hde = $('.hi_' + par);
        hde.attr('src', '/img/plus/' + num + '/' + par + '.jpg');
    }
}
$('.onas_arrow div').click(function(){
    const timeplus = 170;
    var num = $(this).data("where");
    var par = 0;
    var timetoclock = 0;
    for(var i=0; i<4; i++) {
        setTimeout(function(){
        par = par+1;
        var hde = $('.hi_' + par);
        hde.addClass('no-scale');
        },timetoclock);
        timetoclock = timetoclock+timeplus;
    }
//    titlsgoinfo(num);
    setTimeout(function(){$('.hidden_info p').addClass('text-scale');},timetoclock);
    timetoclock = timetoclock+timeplus;
    setTimeout(function(){titlsgoinfo(num);par = 0;},timetoclock);
    timetoclock = timetoclock+timeplus;
    setTimeout(function(){$('.hidden_info p').removeClass('text-scale');},timetoclock);
    for(var i=0; i<4; i++) {
        setTimeout(function(){
            par = par+1;
            var hde = $('.hi_' + par);
            hde.removeClass('no-scale');},
        timetoclock);
        timetoclock = timetoclock+timeplus;
    }
});
$('.titlsgo').click(function(){
    if ($(window).width()<900) {
        const tilt = $('.titlsgo').tilt();
        tilt.tilt.destroy.call(tilt);
        return false;
    }
    yaCounter49117123.reachGoal('titlsgo');
    $('html').addClass('overfloff');
    form_animate('.hidden_info', 'show');
    titlsgoinfo($(this).data('count'));
});


//bonus card

$(document).ready(function(){
    setTimeout(function(){
        form_animate('.bonus_card', 'show');
    }, 10000);
    $('.block_f button').click(function(){
        form_animate('.bonus_card', 'hide');
        form_animate('.bonus_card__2', 'show');
//        $('.block_f').addClass('d-none');
//        $('.block_s').removeClass('d-none');
    });
    $('.hidden_info .closer').click(function(){
        form_animate($('.hidden_info'), 'hide');
        $('html').removeClass('overfloff');
    });
    $('.bc .closer').click(function(){
        yaCounter49117123.reachGoal('close_sale');
        form_animate($(this).parent().parent(), 'hide');
        $('html').removeClass('overfloff');
    });
});
